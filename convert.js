const csvrow = require('csvrow');
const fs = require('fs');

function readCSV(name, skip, map) {
    const csvData = fs.readFileSync('csv/' + name + '.csv', 'utf8');
    const csvLines = csvData.split("\n");

    let results = [];
    let i = 0;
    for (const line of csvLines) 
    {
        if (i++ < skip)
        {
            continue;
        }

        const cells = csvrow.parse(line.trim());
        results.push(map(cells));
    }

    return results;
}

function parseCSVInt(value) {
    try {
        const v = parseInt(value, 10);
        if (isNaN(v)) {
            return null;
        }
        return v;
    } catch {
        return null;
    }
}

function parseCSVFloat(value) {
    try {
        const v = parseFloat(value, 10);
        if (isNaN(v)) {
            return null;
        }
        return v;
    } catch {
        return null;
    }
}

function convertSystems() {
    return readCSV('data_systems', 4, cells => {
        const m = {};
        for (let i = 0; i < 4; i++) {
            const nameIndex = 14 + i;
            const jumpIndex = 20 + i;
            if (cells[nameIndex].trim() === '' || cells[nameIndex].trim() === '#N/A') {
                break;
            }
            const name = cells[nameIndex];
            const jumps = parseCSVInt(cells[jumpIndex]);
            if (jumps !== null) {
                m[name] = jumps;
            }
        }
        return {
            id: cells[0],
            name: cells[1],
            planets: parseCSVInt(cells[2]),
            jumps: {
                promitor: parseCSVInt(cells[3]),
                montem: parseCSVInt(cells[4]),
                katoa: parseCSVInt(cells[5]),
                ...m,
            },
            distance: {
                promitor: parseCSVInt(cells[6]),
                montem: parseCSVInt(cells[7]),
                katoa: parseCSVInt(cells[8]),
            },
            nxt: {
                promitor: parseCSVInt(cells[9]),
                montem: parseCSVInt(cells[10]),
                katoa: parseCSVInt(cells[11]),
            }
        }
    });
}

function convertPlanets() {
    return readCSV('data_planets', 4, cells => {
        return {
            id: cells[0],
            name: cells[1],
            system: cells[2],

            fertility: parseCSVFloat(cells[3]),
            gravity: parseCSVFloat(cells[4]),
            plots: parseCSVInt(cells[5]),
            pressure: parseCSVFloat(cells[6]),
            radius: parseCSVInt(cells[7]),
            temperature: parseCSVInt(cells[8]),
            type: cells[9],

            tier: parseCSVInt(cells[18]),
        }
    });
}

function run() {
    fs.writeFileSync('src/db/data_systems.json', JSON.stringify(convertSystems()));
    fs.writeFileSync('src/db/data_planets.json', JSON.stringify(convertPlanets()));
    console.log('ok');
}

run();