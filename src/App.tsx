import React, { useState, useEffect } from 'react';
import './App.css';
import { SystemModel, PlanetModel } from './models';
import { parseSearchInput, filterSystem, filterPlanet, Term } from './filters';

const systems = require('./db/data_systems.json') as SystemModel[];
const planets = require('./db/data_planets.json') as PlanetModel[];
const systemsByName = new Map<string, SystemModel>(systems.map(x => [x.id, x]));

const ExampleSearch = (props: { children: string, setSearch: (s: string) => void }) => {
  return <a href={"#" + props.children} onClick={() => props.setSearch(props.children)}><code>{props.children}</code></a>
}

function getHashString() {
  let value = decodeURIComponent(window.location.hash);
  if (value.startsWith("#")) {
    value = value.substr(1);
  }
  return value;
}

const useLocationSearch = (): [string, (v: string) => void] => {
  const [int, setInt] = useState(getHashString());
  useEffect(() => {
    const listener = () => {
      setInt(getHashString());
    };
    window.addEventListener('hashchange', listener);
    return () => {
      window.removeEventListener('hashchange', listener);
    };
  })
  return [
    int,
    (value: string) => {
      window.location.hash = value;
      setInt(value);
    }
  ]
}

const App: React.FC = () => {
  const [ search, setSearch ] = useLocationSearch();

  let error = null;
  let terms: Term[] = [];
  try {
    terms = parseSearchInput(search.trim());
  } catch (e) {
    error = e;
  }

  let results = [];
  if (error === null) {
    for (const system of systems) {
      if (results.length > 25) {
        break;
      }
      if (filterSystem(system, terms)) {
        const jumps: JSX.Element[] = [];
        for (const jump of Object.keys(system.jumps)) {
          let jumpName = jump;
          if (jumpName === 'katoa') {
            jumpName = 'Katoa';
          }
          if (jumpName === 'montem') {
            jumpName = 'Montem';
          }
          if (jumpName === 'promitor') {
            jumpName = 'Promitor';
          }
          jumps.push(
            <React.Fragment key={jump}><br /><span>{jumpName}: {system.jumps[jump]}</span></React.Fragment>
          )
        }

        results.push(
          <div className="row" key={"system_" + system.id}>
            <div className="col-12">
              <div className="card mb-2">
                <div className="card-body">
                  <h5 className="card-title">System {system.name || system.id}</h5>
                  <p className="card-text">
                    Planets: {system.planets}<br/><br/>
                    <strong>Jumps:</strong>{jumps}
                  </p>
                </div>
              </div>
            </div>
          </div>
        );
      }
    }
    for (const planet of planets) {
      if (results.length > 25) {
        break;
      }
      if (filterPlanet(planet, terms, systemsByName)) {
        results.push(
          <div className="row" key={"planet_" + planet.id}>
            <div className="col-12">
              <div className="card mb-2">
                <div className="card-body">
                  <h5 className="card-title">Planet {planet.name || planet.id}</h5>
                  <p className="card-text">
                    System: {planet.system}<br />
                    Fertility: {planet.fertility}<br />
                    Gravity: {planet.gravity}<br />
                    Plots: {planet.plots}<br />
                    Pressure: {planet.pressure}<br />
                    Radius: {planet.radius}<br />
                    Temperature: {planet.temperature}<br />
                    Type: {planet.type}<br />
                    Tier: {planet.tier}<br />
                  </p>
                </div>
              </div>
            </div>
          </div>
        );
      }
    }
  }

  let errorBox = null;
  if (error !== null) {
    errorBox = (
      <div className="row mt-5 mb-5">
        <div className="col-12">
      <div className="alert alert-danger" role="alert">
  {error.toString()}
</div></div></div>
    )
  }

  let helpBox = null;
  if (search.trim() === '' || results.length === 0) {
    helpBox = (
      <div className="row mt-5 mb-5" key="helpbox">
        <div className="col-12">
          <div className="card">
            <div className="card-header">
              <strong>Try using these searches</strong>
            </div>
            <table className="table m-0">
              <tbody>
                <tr>
                  <td style={{borderTop: 'none'}}><ExampleSearch setSearch={setSearch}>type:system</ExampleSearch></td>
                  <td style={{borderTop: 'none'}}>Match systems</td>
                </tr>
                <tr>
                  <td><ExampleSearch setSearch={setSearch}>t:planet ptype:gas</ExampleSearch></td>
                  <td>Match has planets (<code>t == type</code>)</td>
                </tr>
                <tr>
                  <td><ExampleSearch setSearch={setSearch}>FI-177a</ExampleSearch> or <ExampleSearch setSearch={setSearch}>&quot;FI-177a&quot;</ExampleSearch></td>
                  <td>Full text search for <code>FI-177a</code> (you can also combine full text search with other filters)</td>
                </tr>
                <tr>
                  <td><ExampleSearch setSearch={setSearch}>jumps.katoa&gt;3</ExampleSearch></td>
                  <td>All systems more than 3 jumps away from Katoa</td>
                </tr>
                <tr>
                  <td><ExampleSearch setSearch={setSearch}>system.jumps.prom&lt;5 fertility&gt;0.1</ExampleSearch></td>
                  <td>Fertile planets less than 5 jumps away from Promitor</td>
                </tr>
                <tr>
                  <td><ExampleSearch setSearch={setSearch}>system.jumps.BE-796&lt;10 radius&gt;20000</ExampleSearch></td>
                  <td>Big planets within 10 jumps of BE-796</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    )
  }

  if (results.length === 0) {
    results = [
      <div className="row mt-5 mb-5" key="noresults">
        <div className="col-12">
          <p className="m-0 p-0">No results found for your query.</p>
        </div>
      </div>
    ];
  }

  return (
    <div className="container">
      <div className="row mt-5 mb-5">
        <div className="col-12">
          <h2>Prosperous DB</h2>
        </div>
      </div>
      <div className="row mt-5 mb-5">
        <div className="col-12">
          <input className="form-control" placeholder="Enter your search query" value={search} onChange={(e) => setSearch(e.target.value)} />
        </div>
      </div>
      {helpBox}
      {errorBox}
      {results}
      <div className="row mt-5 mb-5">
        <div className="col-12">
          <small className="text-muted">All data retrieved from GDP  - Illumilite database (www.GDPPrUn.com).</small>
        </div>
      </div>
    </div>
  );
}

export default App;
