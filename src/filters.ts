import { SystemModel, PlanetModel } from "./models";
import { parse } from "./parser";

export interface Term {
    key: string;
    op: ":" | "<" | ">" | "=" | "!=";
    value: string | number;
}

const reservedKeys = new Set<string>(['type', 't', '_']);

export function parseSearchInput(input: string): Term[] {
    if (input.trim() === '') {
        return [];
    }
    return parse(input) as Term[];
}

function normalize(value: string | number): string | number {
    if (typeof value === 'string') {
        return value.toLowerCase();
    }
    return value;
}

function qMatch(term: Term, value: string | number | null): boolean {
    if (value === null || value === undefined) {
        // we never match queries where the value is unknown
        return false;
    }
    if (term.op === ':') {
        const v = value.toString().toLowerCase();
        const q = term.value.toString().toLowerCase();
        return v.indexOf(q) !== -1;
    }
    if (term.op === '<') {
        return normalize(value) < normalize(term.value);
    }
    if (term.op === '>') {
        return normalize(value) > normalize(term.value);
    }
    if (term.op === '!=') {
        // eslint-disable-next-line
        return normalize(value) != normalize(term.value);
    }

    // eslint-disable-next-line
    return normalize(value) == normalize(term.value);
}

function matchesJsonFilters(obj: any, terms: Term[]) {    
    for (const term of terms) {
        const path = term.key;
        if (reservedKeys.has(path)) {
            continue;
        }

        const components = path.split(".");
        let current = obj;
        let i = 0;
        for (let component of components) {
            if (component === 'prom' && current['promitor'] !== undefined) {
                // promitor is hard to type; let people just use 'prom'
                component = 'promitor';
            }
            if (component === 'ptype' && current['type'] !== undefined) {
                // 'ptype' == planet type
                component = 'type';
            }
            if (current[component] === undefined) {
                return false;
            }
            if (current[component + "_OBJ"] !== undefined && i !== components.length - 1) {
                current = current[component + "_OBJ"]
            } else {
                current = current[component];
            }
        }

        if (!qMatch(term, current)) {
            return false;
        }
    }
    return true;
}

function matchesFullTextObject(obj: any, term: Term) {
    for (const key of Object.keys(obj)) {
        if (!key.endsWith("_OBJ") && qMatch(term, key)) {
            return true;
        }

        if (key === 'jumps') {
            // skip jumps dictionary on systems, because if you're searching
            // for a system name, you probably don't want unrelated systems only
            // attached by jump count
            return false;
        }

        const value = obj[key];
        if (matchesFullTextItem(value, term)) {
            return true;
        }
    }

    return false;
}

function matchesFullTextArray(arr: any, term: Term) {
    for (const item of arr) {
        if (matchesFullTextItem(item, term)) {
            return true;
        }
    }
    return false;
}

function matchesFullTextItem(value: any, term: Term) {
    if (value === undefined || value === null) {
        return false;
    }

    if (typeof value === "string" || typeof value === "number") {
        if (qMatch(term, value)) {
            return true;
        }
    }

    if (Array.isArray(value)) {
        return matchesFullTextArray(value, term);
    }

    if (typeof value === "object") {
        return matchesFullTextObject(value, term);
    }

    return false;
}

function matchesFullTextSearchFilters(obj: any, terms: Term[]) {
    for (const term of terms) {
        if (term.key !== '_') {
            continue;
        }

        if (!matchesFullTextObject(obj, term)) {
            return false;
        }
    }
    return true;
}

function matchesTypeFilter(terms: Term[], req: string): boolean {
    for (const term of terms) {
        if (term.key === 'type' || term.key === 't') {
            // this is a type constraint
            return qMatch(term, req);
        }
    }
    return true;
} 

export function filterSystem(system: SystemModel, terms: Term[]): boolean {
    if (!matchesTypeFilter(terms, 'system')) {
        return false;
    }
    if (!matchesJsonFilters(system, terms)) {
        return false;
    }
    if (!matchesFullTextSearchFilters(system, terms)) {
        return false;
    }

    return true;
}

export function filterPlanet(planet: PlanetModel, terms: Term[], systemsByName: Map<string, SystemModel>): boolean {
    let searchableObject: any = planet;
    if (systemsByName.has(planet.system)) {
        const system = systemsByName.get(planet.system);
        searchableObject = {
            ...planet,
            system_OBJ: system,
        };
    }

    if (!matchesTypeFilter(terms, 'planet')) {
        return false;
    }
    if (!matchesJsonFilters(searchableObject, terms)) {
        return false;
    }
    if (!matchesFullTextSearchFilters(planet, terms)) {
        return false;
    }

    return true;
}