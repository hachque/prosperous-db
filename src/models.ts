export interface SystemModel {
    id: string;
    name: string;
    planets: number | null;
    jumps: {
        katoa: number | null;
        montem: number | null;
        promitor: number | null;
        [system: string]: number | null;
    };
    distance: {
        katoa: number | null;
        montem: number | null;
        promitor: number | null;
    };
    nxt: {
        katoa: number | null;
        montem: number | null;
        promitor: number | null;
    };
}

export interface PlanetModel {
    id: string;
    name: string;
    system: string;
    fertility: number | null;
    gravity: number | null;
    plots: number | null;
    pressure: number | null;
    radius: number | null;
    temperature: number | null;
    type: string;
    tier: number | null;
}